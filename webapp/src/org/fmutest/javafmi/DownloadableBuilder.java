/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.javafmi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipOutputStream;
import static org.fmutest.presentation.web.utils.StreamHelper.*;

public class DownloadableBuilder {

    private List<File> downloadableList;

    public DownloadableBuilder(List<File> downloadableList) {
        this.downloadableList = downloadableList;
    }

    public ByteArrayOutputStream getOutputStream() {
        if (isOnlyOneFile())
            return buildOutputStreamForFile(downloadableList.get(0));
        else
            return buildZipOutputStreamForFiles(new File(getParent(), "logs.zip"));

    }

    private boolean isOnlyOneFile() {
        return (downloadableList.size() == 1);
    }

    private ByteArrayOutputStream buildZipOutputStreamForFiles(File file) {
        createZip(downloadableList);
        return buildOutputStreamForFile(new File(getParent(), "logs.zip"));
    }

    private ByteArrayOutputStream buildOutputStreamForFile(File file) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            FileInputStream fileInputStream = getFileInputStream(file);
            copyData(fileInputStream, outputStream);
            close(fileInputStream);
            return outputStream;
        } catch (IOException ex) {
            Logger.getLogger(DownloadableBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private File createZip(List<File> downloadableList) {
        try {
            ZipOutputStream zipOutputStream = getZipOutputStream(new File(getParent(), "logs.zip"));
            for (File file : downloadableList) {
                addEntryToZip(zipOutputStream, file);
                FileInputStream fileInputStream = getFileInputStream(file);
                copyData(fileInputStream, zipOutputStream);
                close(fileInputStream);
            }
            close(zipOutputStream);
            return new File("logs.zip");
        } catch (IOException ex) {
            Logger.getLogger(DownloadableBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public String getParent() {
        return downloadableList.get(0).getParentFile().getName();
    }
}
