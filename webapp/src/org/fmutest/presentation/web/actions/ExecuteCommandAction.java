/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.actions;

import java.util.List;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.fmutest.javafmi.CommandResult;
import org.fmutest.javafmi.KernelConnection;
import org.fmutest.presentation.web.control.Action;
import org.fmutest.presentation.web.control.constants.Parameter;
import org.fmutest.presentation.web.model.ConnectionStore;

public class ExecuteCommandAction extends Action {

    public ExecuteCommandAction() {
        super();
    }

    @Override
    public String execute() {
        String command = this.request.getParameter(Parameter.COMMAND);
        CommandResult result = null;

        if (command == null)
            throw new RuntimeException("incorrect parameters executing command");

        ConnectionStore connectionStore = ConnectionStore.getInstance();
        String sessionId = request.getSession().getId();

        if (!connectionStore.exists(sessionId))
            throw new RuntimeException("could not execute not existing simulation");

        KernelConnection connection = connectionStore.get(sessionId);
        result = connection.execute(command);

        JSONObject commandResult = result.toJson();
        List<String> logs = connection.getLogsNames();
        JSONArray logsArray = new JSONArray();

        for (String log : logs)
            logsArray.add(log);

        commandResult.put("logs", logsArray);

        return commandResult.toString();
    }
}
