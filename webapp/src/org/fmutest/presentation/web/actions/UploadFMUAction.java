/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.actions;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minidev.json.JSONObject;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.fmutest.javafmi.CommandResult;
import org.fmutest.javafmi.KernelConnection;
import org.fmutest.presentation.web.control.Action;
import org.fmutest.presentation.web.model.ConnectionStore;

public class UploadFMUAction extends Action {

    private static final String ERROR_MESSAGE = "Failure uploading file";

    public UploadFMUAction() {
        super();
    }

    @Override
    public String execute() {
        InputStream fmu = null;
        String fmuId = null;
        JSONObject fmuInfo;
        String sessionId = request.getSession().getId();
        List<?> uploadedFiles = getUploadedFiles();
        if (uploadedFiles == null) return ERROR_MESSAGE;
        Iterator<?> fileListIterator = uploadedFiles.iterator();
        while (fileListIterator.hasNext()) {
            FileItem fileItem = (FileItem) fileListIterator.next();
            if (isFileFromFmufileField(fileItem)) {
                fmuId = sessionId;
                fmu = getInputStream(fileItem);
                if (fmu == null) return ERROR_MESSAGE;
                break;
            }
        }
        ConnectionStore connectionStore = ConnectionStore.getInstance();
        if (connectionStore.exists(sessionId))
            connectionStore.get(sessionId).terminate();
        KernelConnection connection = new KernelConnection(sessionId, fmu);
        CommandResult loadResult = connection.load();
        if (loadResult.hasError)
            return loadResult.result;
        connectionStore.register(sessionId, connection);
        fmuInfo = getFMUInfo(fmuId, connection);
        return fmuInfo != null ? fmuInfo.toString() : "error";
    }

    private List<?> getUploadedFiles() {
        try {
            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            List<?> fileList = uploadHandler.parseRequest(request);
            return fileList;
        } catch (FileUploadException ex) {
            Logger.getLogger(UploadFMUAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private boolean isFileFromFmufileField(FileItem fileItem) {
        return !fileItem.isFormField() && fileItem.getFieldName().equals("fmufile");
    }

    private InputStream getInputStream(FileItem fileItem) {
        try {
            return fileItem.getInputStream();
        } catch (IOException ex) {
            Logger.getLogger(UploadFMUAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
