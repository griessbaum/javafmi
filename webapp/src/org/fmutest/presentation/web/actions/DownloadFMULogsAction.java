/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.actions;

import java.io.ByteArrayOutputStream;
import org.fmutest.javafmi.DownloadableBuilder;
import org.fmutest.javafmi.KernelConnection;
import org.fmutest.presentation.web.control.Action;
import org.fmutest.presentation.web.model.ConnectionStore;

public class DownloadFMULogsAction extends Action {

    public DownloadFMULogsAction() {
        super();
    }

    @Override
    public String execute() {
        String sessionId = request.getSession().getId();

        try {
            ConnectionStore connectionStore = ConnectionStore.getInstance();

            if (!connectionStore.exists(sessionId))
                throw new RuntimeException("could not download logs file");

            KernelConnection connection = connectionStore.get(sessionId);

            try {
                DownloadableBuilder downloadable = connection.getDownloadableLogs();
                ByteArrayOutputStream outputStream = downloadable.getOutputStream();
                boolean compressed = connection.getLogs().size() > 1 ? true : false;

                this.response.setContentLength(outputStream.size());
                this.response.setContentType(compressed ? "application/zip" : "application/vnd.ms-excel");
                this.response.setHeader("Content-Disposition", "attachment; filename=log" + (compressed ? ".zip" : ".csv"));
                this.response.getOutputStream().write(outputStream.toByteArray());
                this.response.getOutputStream().flush();

            } catch (Exception exception) {
                throw new RuntimeException("could not download logs file");
            }

        } catch (Exception oException) {
            return "could not download logs file";
        }

        return null;
    }
}
