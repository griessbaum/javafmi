/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class StreamHelper {

    public static void close(InputStream stream) {
        if (stream != null)
            try {
                stream.close();
            } catch (Exception e) {
            }
    }

    public static void close(OutputStream stream) {
        if (stream != null)
            try {
                stream.flush();
                stream.close();
            } catch (Exception e) {
            }
    }

    public static void close(Reader reader) {
        if (reader != null)
            try {
                reader.close();
            } catch (Exception e) {
            }
    }

    public static void close(Writer writer) {
        if (writer != null)
            try {
                writer.close();
            } catch (Exception e) {
            }
    }

    public static void copyData(InputStream input, OutputStream output) throws IOException {
        int len;
        byte[] buff = new byte[16384];
        while ((len = input.read(buff)) > 0)
            output.write(buff, 0, len);
    }

    public static void addEntryToZip(ZipOutputStream zip, File file) {
        try {
            zip.putNextEntry(new ZipEntry(file.getName()));
        } catch (IOException ex) {
            Logger.getLogger(StreamHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static InputStream getInputStreamForEntry(ZipFile zip, ZipEntry entry) {
        try {
            return zip.getInputStream(entry);
        } catch (IOException ex) {
            Logger.getLogger(StreamHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ZipOutputStream getZipOutputStream(File zipFile) {
        try {
            return new ZipOutputStream(new FileOutputStream(zipFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StreamHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static FileInputStream getFileInputStream(File file) {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StreamHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public static FileOutputStream getFileOutputStream(File file) {
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StreamHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
