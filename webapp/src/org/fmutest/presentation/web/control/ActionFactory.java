/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.control;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fmutest.presentation.web.actions.DownloadFMULogsAction;
import org.fmutest.presentation.web.actions.ExecuteCommandAction;
import org.fmutest.presentation.web.actions.LoadFMUAction;
import org.fmutest.presentation.web.actions.UnloadFMUAction;
import org.fmutest.presentation.web.actions.UploadFMUAction;

public class ActionFactory {

    private static ActionFactory instance;
    private HashMap<String, Class<? extends Action>> actionsMap;
    public static final String LOAD_FMU = "loadfmu";
    public static final String UNLOAD_FMU = "unloadfmu";
    public static final String UPLOAD_FMU = "uploadfmu";
    public static final String EXECUTE_COMMAND = "executecommand";
    public static final String DOWNLOAD_FMU_LOGS = "downloadfmulogs";

    static {
        ActionFactory.getInstance().register(LOAD_FMU, LoadFMUAction.class);
        ActionFactory.getInstance().register(UNLOAD_FMU, UnloadFMUAction.class);
        ActionFactory.getInstance().register(UPLOAD_FMU, UploadFMUAction.class);
        ActionFactory.getInstance().register(EXECUTE_COMMAND, ExecuteCommandAction.class);
        ActionFactory.getInstance().register(DOWNLOAD_FMU_LOGS, DownloadFMULogsAction.class);
    }

    private ActionFactory() {
        this.actionsMap = new HashMap<String, Class<? extends Action>>();
    }

    public synchronized static ActionFactory getInstance() {
        if (instance == null) instance = new ActionFactory();
        return instance;
    }

    public Action get(String type, HttpServletRequest request, HttpServletResponse response) {
        Class<?> actionClass;
        Action action = null;

        try {
            actionClass = (Class<?>) this.actionsMap.get(type);
            action = (Action) actionClass.newInstance();
            action.setRequest(request);
            action.setResponse(response);
            action.initialize();
        } catch (NullPointerException oException) {
            throw new RuntimeException("ActionFactory error: " + type, oException);
        } catch (Exception oException) {
            throw new RuntimeException("ActionFactory error: " + type, oException);
        }
        return action;
    }

    public void register(String type, Class<? extends Action> actionClass) {
        this.actionsMap.put(type, actionClass);
    }
}
