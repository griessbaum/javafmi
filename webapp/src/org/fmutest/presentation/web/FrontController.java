/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fmutest.presentation.web.control.Action;
import org.fmutest.presentation.web.control.ActionFactory;
import org.fmutest.presentation.web.control.constants.Parameter;

public class FrontController extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

    static final long serialVersionUID = 1L;

    public FrontController() {
        super();
    }

    @Override
    public void init(ServletConfig oConfiguration) throws ServletException {
        super.init(oConfiguration);
    }

    private String getOperation(HttpServletRequest oRequest) {
        return oRequest.getParameter(Parameter.OPERATION);
    }

    private Boolean doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String result, operation;
        Action action;

        try {

            if ((operation = this.getOperation(request)) == null) {
                response.getWriter().print("Operation not found: " + operation);
                return false;
            }

            action = ActionFactory.getInstance().get(operation, request, response);
            result = action.execute();
        } catch (Exception exception) {
            response.getWriter().print(exception.getMessage());
            return false;
        }

        if (result != null)
            response.getWriter().printf(result);

        return true;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doRequest(request, response);
    }
}