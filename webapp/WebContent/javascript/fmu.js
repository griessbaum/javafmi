// LIB FUNCTIONS
function isFunction(item) {
    return (typeof item == "function");
}

function serializeParameters(parameters) {
    var result = "";
    for (var index in parameters) {
        if (isFunction(parameters[index])) continue;
        result += "&" + index + "=" + parameters[index];
    }
    return result;
}

function htmlEncode(content) {
    var jElement = $("<div/>");
    var result = jElement.text(content).html();
    jElement.remove();
    return result;
}

function serializeArrayToHtmlList(array) {
    var result = "";
    for (var key in array) {
        if (isFunction(key)) continue;
        result += "<li>" + htmlEncode(array[key]) + "</li>";
    }
    return result;
}

function getTemplate(name) {
    return $(".config .templates ." + name).html();
}

// MODEL
var State = {};
State.fmuId = null;

// EVENTS
function registerEvents() {
    $("#fmufile").change(uploadFMUAction.bind(this));
    $("#commandbox").keypress(parseCommandAction.bind(this));
    $("#commandbutton").click(executeCommandAction.bind(this));
    window.onbeforeunload = unloadFMUAction.bind(this);
}

function showHome() {
    unloadFMUAction();
    $(".section.upload").show();
    $(".section.console").hide();
}

// KERNEL
var Kernel = {};

Kernel.readUrl = function() { 
    return $(".config .url").html(); 
};

Kernel.request = function(command, parameters, callback) {
    var url = Kernel.readUrl() + "?op=" + command + ((parameters != null)?serializeParameters(parameters):"");
    $.ajax({
        url: url,
        complete: Kernel.atResponse.bind(this, command, parameters, callback)
    });
};

Kernel.atResponse = function(command, parameters, callback, response) {
  
    if (response.status == 200) {
        try {
            callback(response.responseText);
        }
        catch(e) { 
            alert(e.message); 
        }
    }
    else if (response.status != 0) {
        alert("Connection failure with server");
    }
};

Kernel.loadFMU = function(callback) {
    Kernel.request("loadfmu", {}, callback);
};

Kernel.unloadFMU = function() {
    Kernel.request("unloadfmu", {}, function(){});
};

Kernel.uploadFMU = function(callback) {
    var fileData = $("#fmufile").prop("files")[0];
    var formData = new FormData();
    formData.append("fmufile", fileData);
    $.ajax({
        url: Kernel.readUrl() + "?op=uploadfmu",
        complete: Kernel.atResponse.bind(this, "uploadfmu", null, callback),
        dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        type: 'post'
    });
};

Kernel.executeCommand = function(fmuId, command, callback) {
    Kernel.request("executecommand", {
        fmu: fmuId, 
        command: command
    }, callback);
};

Kernel.getDownloadFMULogsUrl = function() {
    return Kernel.readUrl() + "?op=downloadfmulogs";
};

// VIEW
function showFMU(fmuInfo) {
    $(".section.upload").hide();
    $(".section.console").show();
    $(".section.console .messages").html(""); 
    $(".section.console .model.name").html(fmuInfo.modelName);
    $(".section.console .model.commands").html(serializeArrayToHtmlList(fmuInfo.commands));
    $(".section.console .model.variables").html(serializeArrayToHtmlList(fmuInfo.variables));
}

function showDownloadFMULogs() {
    var jMessage = $(getTemplate("messageitem_download"));
    var jMessages = $(".section.console .messages"); 
    
    jMessage.find(".downloadlink").attr("href", Kernel.getDownloadFMULogsUrl());
    jMessages.append(jMessage);
}

// ACTIONS
function loadFMUAction() {
    Kernel.loadFMU(loadFMUActionCallback);
}
function loadFMUActionCallback(serializedMessage) {
    if (serializedMessage == "") {
        $(".section.upload").show();
        return;
    }

    var fmuInfo = jQuery.parseJSON(serializedMessage);
    State.fmuId = fmuInfo.id;
 
    showFMU(fmuInfo);
}

function unloadFMUAction() {
    Kernel.unloadFMU();
}

function uploadFMUAction() {
    Kernel.uploadFMU(uploadFMUActionCallback);
}
function uploadFMUActionCallback(serializedMessage) {
    $(".dialog .errormessage").hide();
  
    if (serializedMessage == "error")
        $(".dialog .errormessage").show();

    var fmuInfo = jQuery.parseJSON(serializedMessage);
    State.fmuId = fmuInfo.id;
    
    $("#fmufile").val("");
 
    showFMU(fmuInfo);
}

function parseCommandAction(event) {
    if (event.which == 13)
        executeCommandAction();
}

function executeCommandAction() {

    if (State.fmuId == null)
        alert("Upload a valid fmu file");
  
    var command = $("#commandbox").val();
    if (command == "") {
        return;
    }
  
    $("#commandbutton").attr('disabled', 'disabled');
    Kernel.executeCommand(State.fmuId, command, executeCommandActionCallback);
}
function executeCommandActionCallback(serializedMessage) {
    var message = jQuery.parseJSON(serializedMessage);
    var jMessages = $(".section.console .messages"); 
  
    $("#commandbutton").attr('disabled', null);
    $("#commandbox").val('');
  
    var jMessage = $(getTemplate("messageitem"));
    jMessages.append(jMessage);

    jMessage.find(".command").html(message.command);
    jMessage.find(".result").html(message.result);
    
    if (message.error)
        jMessage.find(".result").addClass("error");
  
    if (message.logs.length > 0)
        showDownloadFMULogs();

    jMessages.animate({
        scrollTop : jMessage.position().top
    }, "slow");
}


// MAIN
$(document).ready(
    function(){
        registerEvents();
        loadFMUAction();
        $("#commandbox").val("");
    }
    );