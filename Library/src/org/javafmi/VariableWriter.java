/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javafmi.wrapper.FmuProxy;

public class VariableWriter {

    private static VariableWriter instance;
    private static final Map<Class, Method> setterMethodsByClass;

    static {
        setterMethodsByClass = new HashMap<>();
        try {
            setterMethodsByClass.put(Double.class, FmuProxy.class.getMethod("setReal", Integer.class, Double.class));
            setterMethodsByClass.put(Integer.class, FmuProxy.class.getMethod("setInteger", Integer.class, Integer.class));
            setterMethodsByClass.put(Boolean.class, FmuProxy.class.getMethod("setBoolean", Integer.class, Boolean.class));
            setterMethodsByClass.put(String.class, FmuProxy.class.getMethod("setString", Integer.class, String.class));
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(VariableWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private VariableWriter() {
    }

    public static VariableWriter getInstance() {
        if (instance == null)
            instance = new VariableWriter();
        return instance;
    }

    public Status write(String name, Object value, FmuProxy fmuProxy) {
        Integer valueReference = fmuProxy.getModelDescription().getScalarVariable(name).getValueReference();
        try {
            return (Status) setterMethodsByClass.get(value.getClass()).invoke(fmuProxy, valueReference, value);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(VariableWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
