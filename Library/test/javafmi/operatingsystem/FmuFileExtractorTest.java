/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.operatingsystem;

import java.io.File;
import org.javafmi.operatingsystem.FmuFileExtractor;
import org.javafmi.operatingsystem.PathBuilder;
import org.junit.Assert;
import org.junit.Test;

public class FmuFileExtractorTest {

    private String fileDirectory = PathBuilder.buildPath(new String[]{"resources", "fmu", "cosimulation"}, "bouncingBall.fmu");
    private String outDirectoryWindows = PathBuilder.buildPath("tmp", "bouncingBall.dll");
    private String outDirectoryDarwin = PathBuilder.buildPath("tmp", "bouncingBall.dylib");
    private String outDirectoryLinux = PathBuilder.buildPath("tmp", "bouncingBall.so");
    private String modelName = "bouncingBall";

    @Test
    public void testExtractingModelDescription() {
        FmuFileExtractor fmuFileExtractor = new FmuFileExtractor(new File(fileDirectory));
        File modelDescriptionFile = fmuFileExtractor.getModelDescriptionFile();
        Assert.assertNotNull(modelDescriptionFile);
    }

    @Test
    public void testExtractingLibrary() {
        FmuFileExtractor fmuFileExtractor = new FmuFileExtractor(new File(fileDirectory));
        String libraryFile = fmuFileExtractor.getLibraryPath(modelName);
        Assert.assertNotNull(libraryFile);
        if (systemIsMac())
            Assert.assertEquals(outDirectoryDarwin, libraryFile);
        else if (systemIsWindows())
            Assert.assertEquals(outDirectoryWindows, libraryFile);
    }

    private boolean systemIsMac() {
        return System.getProperty("os.name").toLowerCase().startsWith("mac");
    }

    private boolean systemIsWindows() {
        return System.getProperty("os.name").toLowerCase().startsWith("win");
    }
}
