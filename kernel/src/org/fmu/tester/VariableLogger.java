/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class VariableLogger {

    public static String pathToLog;
    private String variableName;
    private BufferedWriter bufferedWriter;

    public VariableLogger(String variableName) {
        try {
            this.variableName = variableName;
            this.bufferedWriter = new BufferedWriter(new FileWriter(pathToLog + "/" + variableName + ".csv"));
            this.bufferedWriter.write("Time(s);" + this.variableName + "\n");
        } catch (Exception e) {
        }
    }

    public void log(double time, String value) {
        try {
            String string = time + ";" + value + "\n";
            bufferedWriter.write(string.replace(".", ","));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void terminate() {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getVariableName() {
        return variableName;
    }
}
