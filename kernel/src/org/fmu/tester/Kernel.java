/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester;

import java.io.File;

public class Kernel {

    private final Simulation simulation;
    private VariableLoggerList variableLoggerList = new VariableLoggerList();
    private File workingDirectory;

    public Kernel(String fmuPath) {
        try {
            this.workingDirectory = new File(fmuPath).getParentFile();
            this.simulation = new Simulation(new org.javafmi.Simulation(fmuPath));
            VariableLogger.pathToLog = workingDirectory.getAbsolutePath();
        } catch (Exception e) {
            throw new RuntimeException("Problem loading the fmu located at: " + fmuPath + ". Right path?");
        }
    }

    public String executeCommand(String in) {
        Command command = CommandFactory.createCommand(in, variableLoggerList);
        if (command == null)
            return "Unknown command";
        return command.execute(simulation);
    }

    public String getModelName() {
        return simulation.getInternalName();
    }

    public String[] listVariables() {
        return simulation.listVariables().toArray(new String[0]);
    }

    public boolean isTerminated() {
        return simulation.isFinished();
    }
}